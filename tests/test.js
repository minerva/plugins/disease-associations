const {Builder, By, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

const assert = require('chai').assert;

const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

// Some tests need to access the MINERVA proxy to, e.g., check which elements are highlighted. However, the tests
// do not run in the same scope as the plugin and thus they do not have access to the Proxy. Therefore, the plugin
// exposes the proxy by attaching it as a data attribute to the main div element.
const pluginName = 'disease-associations';
const pluginLabel = 'Disease-variant associations';
const minervaProxyContainerClass = pluginName + '-container';
const minervaProxyCode = `$('.${minervaProxyContainerClass}').data('minervaProxy')`;


function minervaLogin() {

    const xhr = new XMLHttpRequest();

    return new Promise(function (resolve, reject) {

        xhr.onreadystatechange = function () {

            if (xhr.readyState !== 4) return;

            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.open("POST", 'http://localhost:8080/minerva/api/doLogin', false);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.send("login=admin&password=admin");
    });
}

async function getRequest(uri) {

    const xhr = new XMLHttpRequest();

    return new Promise(function (resolve, reject) {

        xhr.onreadystatechange = function () {

            if (xhr.readyState !== 4) return;

            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr);
            } else {
                reject({
                    status: xhr.status,
                    statusText: xhr.statusText
                });
            }
        };

        xhr.open("GET", uri);
        xhr.send();
    });
}

async function getPluginHash(){
    return getRequest('http://localhost:8080/minerva/api/plugins/').then(function (pluginsResponse) {
        let hashes = JSON.parse(pluginsResponse.responseText).filter(plugin => plugin.name === pluginLabel);
        if (hashes.length === 0){
            // when tested withing CI there is only one plugin, the current one and it's name is test
            hashes = JSON.parse(pluginsResponse.responseText);
        }
        return hashes[hashes.length -1].hash;
    });
}

describe("disease-association plugin", async function() {

    //Some functions can take a lot of time as they need, for isntance, start MINERVA interface
    this.timeout(60000);

    let driver;
    let minervaProxy;
    let pluginContainer;

    function wait(timeInMs) {
        return driver.executeAsyncScript(`
            const callback = arguments[arguments.length - 1];
            setTimeout(()=>callback(), ${timeInMs})`);
    }

    function deHighlightAll(){
        return driver.executeScript(`minervaProxy = ${minervaProxyCode}; minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) )`);
    }

    async function getHighlighted(){
        return driver.executeAsyncScript(`
                    var callback = arguments[arguments.length - 1];
                    ${minervaProxyCode}.project.map.getHighlightedBioEntities().then(highlighted => callback(highlighted));                    
                `);
    }

    before(async function () {
        const opts = new chrome.Options().addArguments('--no-sandbox', '--headless', '--remote-debugging-port=9222');
        driver = await new Builder().setChromeOptions(opts).forBrowser('chrome').build();
        // driver = await new Builder().forBrowser('chrome').build();

        await driver.manage().window().maximize();

        const loginResponse = await minervaLogin();
        const minervaToken = JSON.parse(loginResponse.responseText).token;

        await driver.get('http://localhost:8080');
        await driver.manage().addCookie({name: 'MINERVA_AUTH_TOKEN', value: minervaToken});
        const pluginHash = await getPluginHash();

        await driver.get(`http://localhost:8080/minerva/index.xhtml?id=single-map&plugins=${pluginHash}`);

        pluginContainer = await driver.wait(until.elementLocated(By.css(`.${minervaProxyContainerClass}`)),10000);
        await driver.wait(until.elementLocated(By.css(`.disease-input-group .input-disease`)),15000);
    });

    describe("Parkinson's disease by EFO", function () {
        before(async function () {
            const input = driver.findElement(By.css('.disease-input-group .input-disease'));
            input.sendKeys('parkinson');
            await driver.wait(async function(){
                const display = await driver.executeScript(function (){
                    return $('.disease-input-group .tt-menu.tt-open').css("display")
                });
                return display === "block";
            }, 30000);
            await wait(1000);
        });

        it("should show parkinson's disease", async function f() {
            await driver.executeScript(function () {
                $($('.tt-suggestion.tt-selectable:contains("\'s disease")')[0]).click()
            });
            const synonyms = await driver.wait(until.elementLocated(By.css('.disease-info.disease-synonyms .disease-info-val')));
            await driver.wait(until.elementTextMatches(synonyms, new RegExp('.*Park.*')));
            assert.include(await synonyms.getText(), "Parkinson's syndrome");
        });

        describe("retrieve", function () {
            before(async function () {
                driver.executeScript(function () {
                    $('.disease-associations-container button.btn-search').click();
                });
                await driver.wait(async function () {
                    return await driver.executeScript(function () {
                        return $('table.variation-table td:contains("LRRK2")').length;
                    }) > 0;
                }, 10000);
                // await driver.wait(until.elementLocated(By.css('.associations-content .bootstrap-table td')),10000);
            });

            describe("variation", function () {
                it("should have LRRK2 and PRKN associated", async function() {
                    const cnt = await driver.executeScript(function () {
                        return $('table.variation-table td:contains("LRRK2"),table.variation-table td:contains("PRKN")').length
                    });
                    assert.equal(cnt, 2);
                });

                it("should contain variation details", async function () {
                    driver.executeScript(function () {
                        $('table.variation-table td:contains("LRRK2")').parent().find('.detail-icon').click();
                    });
                    await wait(200);
                    await driver.wait(until.elementLocated(By.css('table.variation-table tr.detail-view li')),10000);
                    const genes = await driver.findElements(By.css('table.variation-table tr.detail-view li'));
                    assert.isAbove(genes.length, 1);
                });

                it("should show variants", async function () {
                    driver.executeScript(function () {
                        $($('table.variation-table a.detail-icon i.fa-plus')[0]).parent().click();
                    });
                    await driver.wait(until.elementLocated(By.css('#tree-LRRK2-Homo_sapiens li')), 2000);
                    const variants = await driver.findElements(By.css('table.variation-table tr.detail-view li'));
                    assert.isAbove(variants.length, 1);

                    const plus = await variants[0].findElements(By.css('.fa-plus'));
                    plus[0].click();
                    await driver.wait(until.elementLocated(By.css('#tree-LRRK2-Homo_sapiens span.indent')), 2000);
                    const detail = await driver.executeScript(function () {
                        return $('#tree-LRRK2-Homo_sapiens span.indent').parent().find('a')[0];
                    });
                    assert.include(await detail.getText(), 'A0A1B0GUQ3');
                });

                describe("clicking on LRRK", function () {
                    before(async function () {
                        const markers  = await driver.wait(until.elementsLocated(By.css('table.variation-table .fa-map-marker-alt')));
                        markers[0].click();
                        await wait(500);
                    });

                    it("should highlight in map", async function () {
                        highlighted = await getHighlighted();
                        assert.equal(highlighted.length, 2);
                    });

                })
            });

            describe("opentargets", function () {
                it("should have LRRK2 and PRKN associated", async function() {
                    const cnt = await driver.executeScript(function () {
                        return $('table.opentargets-table td:contains("LRRK2"),table.opentargets-table td:contains("PRKN")').length
                    });
                    assert.equal(cnt, 2);
                });
            });
        })


    });

    describe("Parkinson's disease by OMIM", function () {
        before(async function () {
            await driver.executeScript(function(){
                $('.disease-input-group input.omim').click();
            });
            await wait(500);
            await driver.executeScript(function(){
                $('.disease-input-group .input-disease').val('');
            });
            await wait(100);
            driver.findElement(By.css('.disease-input-group .input-disease')).sendKeys('168601');
            await wait(500);
            driver.executeScript(function () {
                $('.disease-associations-container button.btn-search').click();
            });
            await driver.wait(async function () {
                return await driver.executeScript(function () {
                    return $('table.variation-table td:contains("SNCA")').length;
                }) > 0;
            }, 10000);
        });

        describe("variation", function () {
            it("should have SNCA associated", async function () {
                const cnt = await driver.executeScript(function () {
                    return $('table.variation-table td:contains("SNCA")').length
                });
                assert.equal(cnt, 1);
            });
        });
    });

    after(async function finishWebDriver() {
        await driver.quit();
    });
});