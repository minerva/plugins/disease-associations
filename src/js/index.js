require('../css/styles.css');

// window.$ = window.jQuery = require('jquery');

if (window.$ === undefined && minerva.$ !== undefined) {
    window.$ = window.jQuery = window.jquery = minerva.$;
}


// require('popper.js');
// require('bootstrap/dist/js/bootstrap.min');
require('bootstrap-treeview');
require('bootstrap-table');


if ($.fn.typeahead === undefined) {
    require("../assets/typeahead.jsquery.min");
}


const ols = require('./ols');

const linkify = require("html-linkify");

const pluginName = 'disease-associations';
const pluginLabel = 'Disease-variant associations';
const pluginVersion = '1.0.0';
const containerName = pluginName + '-container';


function isArray (value) {
    return value && typeof value === 'object' && value.constructor === Array;
}

class SelectedDisease {
    constructor() {
        this.clear();
    }

    clear() {
        this.efo =  undefined;
        this.iri = undefined;
        this.label =  undefined;
        this.omims =  [];
        this.synonyms =  [];

        if (pluginContainer){

            pluginContainer.find('.disease-info.disease-name .disease-info-val').text('');
            pluginContainer.find('.disease-info.disease-synonyms .disease-info-val').text('');
            pluginContainer.find('.disease-info.disease-omim .disease-info-val').text('');

        }
    }

    setIri(disease) {

        this.iri = disease.iri;
        this.label = disease.label;
        const nameHtml = `<a href="${this.iri}" target="_blank">${disease.label}</a>`;
        pluginContainer.find('.disease-info.disease-name .disease-info-val').html(nameHtml);
    }
    getIri(){return this.iri};

    setEfo(disease) {
        this.efo = disease.obo_id;
    }
    getEfo(){return this.efo};

    setSynonyms(disease) {

        this.synonyms = 'synonyms' in disease && disease.synonyms !== null ? disease.synonyms : [];
        pluginContainer.find('.disease-info.disease-synonyms .disease-info-val').text(this.synonyms.join(', '));
    }
    getSynonyms(){return this.synonyms};

    setOmims(diseaseOrArray) {

        if (isArray(diseaseOrArray)) {
            this.omims = diseaseOrArray;
        } else {
            this.omims = 'database_cross_reference' in diseaseOrArray.annotation ? diseaseOrArray.annotation.database_cross_reference
                .filter(cr => cr.startsWith('OMIM:'))
                .map(omim => omim.split(':')[1]) : [];
        }
        const omimsHtml = this.omims.map(o => `<a href="https://www.omim.org/entry/${o}" target="_blank">${o}</a>`);
        pluginContainer.find('.disease-info.disease-omim .disease-info-val').html(omimsHtml.join(', '));
    }
    getOmims(){return this.omims};
}

const globals = {
    selected: [],
    allBioEntities: [],
    pickedRandomly: undefined,
    goupedData: undefined,
    expanded: false,
    gene2BioEntities: {},
    diseaseSuggestionsCache: {},
    lastSuggestions: {},
    selectedDisease: new SelectedDisease(),
    searching: false
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;
let variationContainer;
let opentargetsContainer;
let minervaVersion;


const register = function(_minerva) {

    // console.log('registering ' + pluginName + ' plugin');

    $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);

    pluginContainerId = pluginContainer.attr('id');
    if (!pluginContainerId) {
        //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
        pluginContainer.css('height', '100%');
        pluginContainerId = pluginContainer.parent().attr('id');
    }


    // console.log('minerva object ', minervaProxy);
    // console.log('project id: ', minervaProxy.project.data.getProjectId());
    // console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    initPlugin().then(function () {
        pluginContainer.find(`.${containerName}`).data("minervaProxy", minervaProxy);
    });
};

const unregister = function () {
    // console.log('unregistering ' + pluginName + ' plugin');

    unregisterListeners();
    return deHighlightAll();
};

const getName = function() {
    return pluginLabel;
};

const getVersion = function() {
    return pluginVersion;
};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion
        ,minWidth: 300
        ,defaultWidth: 600
    }
});

function initPlugin () {
    registerListeners();
    const container = initMainContainer();
    return minerva.ServerConnector.getConfiguration().then( function(conf) {
        minervaVersion = parseFloat(conf.getVersion().split('.').slice(0, 2).join('.'));
        return initGene2BioEntities();
    })
    .then(function(){
        container.find('.genes-loading').hide();
        initMainPageStructure(container);
    });


}

function registerListeners(){
    // minervaProxy.project.map.addListener({
    //     dbOverlayName: "search",
    //     type: "onSearch",
    //     callback: searchListener
    // });
}

function unregisterListeners() {
    // minervaProxy.project.map.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


function deHighlightAll(){
    return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function getContainerClass() {
    return pluginName + '-container';
}

function getVariationTabId(){
    return `${pluginContainerId}-variation-tab`;
}

function getOpenTargetsTabId(){
    return `${pluginContainerId}-opentargets-tab`;

}

function initMainContainer(){
    const container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
    container.append(`
    <div class="panel panel-default genes-loading">
        <div class="panel-body">  
            <i class="fa fa-circle-o-notch fa-spin"></i> Obtaining HGNC-based gene-bioentities mapping from the map
        </div>        
    </div>`);

    return container;
}

function initMainPageStructure(container){

    const createDiseaseInputGroup = function() {
        if (minervaVersion < 14) {
            return `
                <form class="form-horizontal" onSubmit="return false;">
                    <div class="form-group">
                        <label class="col-sm-2 control-label form-control-sm">Disease: </label>
                        <div class="col-sm-7">
                            <input class="form-control-static input-disease input-disease-v14-less typeahead input-sm" type="text" placeholder="Disease name">
                        </div>
                        <div class="col-sm-3">
                            <div class="btn-group" role="group" data-toggle="buttons">
                                <label class="btn btn-default btn-on btn-sm active" title="EFO ontology - input disease name">
                                <input type="radio" value="1" class="efo" checked="checked">EFO</label>
                                <label class="btn btn-default btn-off btn-sm " title="OMIM ontology - input OMIM ID of the disease">
                                <input type="radio" value="0" class="omim">OMIM</label>
                            </div>                        
                        </div>
                    </div>                                        
                </form>`;
        } else {
            return `
                <div class="form-group row disease-input-group ">
                    <label  class="col-sm-2 col-form-label form-control-sm font-weight-bold pl-4">Disease:</label>
                    <div class="col-7">
                        <input type="text" class="form-control typeahead input-disease input-sm form-control-sm" placeholder="Disease name">                    
                    </div>                    
                    <div class="col-3">
                        <i class="fa fa-circle-o-notch fa-spin d-none suggesting"></i>
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                             <label class="btn btn-default btn-sm btn-on active" title="EFO ontology - input disease name">
                                <input type="radio" value="1" class="efo" checked="checked">EFO
                             </label>
                             <label class="btn btn-default btn-sm btn-off" title="OMIM ontology - input OMIM ID of the disease">
                                <input type="radio" value="0" class="omim">OMIM
                             </label>
                        </div>
                    </div>                
                </div>
              
            `;
        }
    };

    const createAssociationsTabs = function() {
        if (minervaVersion < 14) {
            return `
                <div class="associations-content tab-content">
                <div id="${getVariationTabId()}" class="tab-pane fade in active">
                    <div class="panel panel-danger error-panel hidden">
                        <div class="panel-heading">
                            Error
                        </div> 
                        <div class="panel-body">                         
                        </div>                  
                    </div>            
                </div>
                <div id="${getOpenTargetsTabId()}" class="tab-pane fade">
                    <div class="panel panel-danger error-panel hidden">
                        <div class="panel-heading">
                            Error
                        </div> 
                        <div class="panel-body">                         
                        </div>                  
                    </div>            
                </div>
            </div>
            `;

        } else {
            return `
                <div class="associations-content tab-content">
                    <div id="${getVariationTabId()}" class="tab-pane fade show in active">
                        <div class="panel card border-danger panel-danger error-panel hidden d-none">
                            <div class="panel-heading card-header bg-danger text-light">
                                Error
                            </div> 
                            <div class="panel-body card-body">                         
                            </div>                  
                        </div>            
                    </div>
                    <div id="${getOpenTargetsTabId()}" class="tab-pane fade">
                        <div class="panel card panel-danger border-danger error-panel hidden d-none">
                            <div class="panel-heading card-header bg-danger text-light">
                                Error
                            </div> 
                            <div class="panel-body card-body">                         
                            </div>                  
                        </div>            
                    </div>
                </div>
            `;
        }
    };

    container.append(`
        <div class="disease-submission">
            ${createDiseaseInputGroup()}
            <div class="panel card panel-default disease-infos small">
                <div class="panel-heading card-header" data-toggle="collapse" data-target="#disease_panel_${pluginContainerId}">
                    Disease details
                    <span class="pull-right fa fa-chevron-down">
                </div>
                <div id="disease_panel_${pluginContainerId}" class="panel-body card-body panel-default panel-collapse collapse">
        <!--<div class="disease-infos small">-->
                    <div class="disease-info disease-name">
                        <div class="row">
                            <div class="col-sm-2 disease-info-label text-right">Canonical: </div>
                            <div class="col-sm-10 disease-info-val"></div>
                        </div>
                    </div>
                    <div class="disease-info disease-synonyms">
                        <div class="row">
                            <div class="col-sm-2 disease-info-label text-right">Synonyms: </div>
                            <div class="col-sm-10 disease-info-val"></div>
                        </div>
                    </div>
                    <div class="disease-info disease-omim">
                        <div class="row">
                            <div class="col-sm-2 disease-info-label text-right">OMIM: </div>
                            <div class="col-sm-10 disease-info-val"></div>
                        </div>
                    </div>
                </div>
                     
            </div>
        </div>
        <button type="button" class="btn-search btn btn-success btn-default btn-block">${searching(false)}</button>
        
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="nav-item" role="tab">
                <a class="nav-link active" data-toggle="tab" href="#${getVariationTabId()}">Variation</a>
            </li>
            <li role="presentation" class="nav-item opentargets-tab" role="tab">
                <a class="nav-link" data-toggle="tab" href="#${getOpenTargetsTabId()}">Open Targets</a>
            </li>          
        </ul>
        
        ${createAssociationsTabs()}
        
        <div class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Error</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning">No disease selected.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div> 
    `);

    pluginContainer.find('.input-disease.typeahead').typeahead({
        hint: false,
        highlight: true,
        minLength: 2
        },
        {
            name: 'diseases',
            source: typeaheadSource
            , limit: 30
        });

    pluginContainer.find('.typeahead').bind('typeahead:select', (ev, suggestion) => typeaheadSelect(ev, suggestion));

    container.find('.btn-search').on('click', () => search() );

    // container.find('.input-disease').kepress(function (e){
    //
    //     if (e.which == 13) {
    //         e.preventDefault();
    //         return false;
    //     }
    // });

    container.find('.input-disease').keyup(function (e){
        if (e.which !== 13) {
            globals.selectedDisease.clear();
        }

        if (!efoOntologySelected()) {
            globals.selectedDisease.setOmims([$(e.target).val()])
        }
    });

    pluginContainer.find('.disease-submission .panel-body').on('hidden.bs.collapse', () => setTablesHeight());
    pluginContainer.find('.disease-submission .panel-body').on('shown.bs.collapse', () => setTablesHeight());

    variationContainer = pluginContainer.find(`#${getVariationTabId()}`);
    opentargetsContainer = pluginContainer.find(`#${getOpenTargetsTabId()}`);

    pluginContainer.find('.disease-submission label').click(() => diseaseOntologyChanged());
}

const diseaseOntologyChanged = function(){
    const efo = efoOntologySelected();

    const placeholder = efo ? "OMIM ID" : "Disease name"; //happens before the active class is actually changed
    pluginContainer.find('.input-disease').attr('placeholder', placeholder);

};

const efoOntologySelected = function () {
    return pluginContainer.find('.disease-submission .efo').parent().hasClass('active');
};

const hideDiseaseDetails = function () {
    pluginContainer.find('.disease-infos .panel-body').collapse('hide');
};

const hideErrorPanels = function () {
    pluginContainer.find('.associations-content .error-panel').removeClass('hidden').addClass('hidden');
    pluginContainer.find('.associations-content .error-panel').removeClass('d-none').addClass('d-none');
};

const showDiseaseDetails = function () {
    pluginContainer.find('.disease-infos .panel-body').collapse('show');
};

const typeaheadSource = function(query, syncResults, asyncResults){

    if (!efoOntologySelected()) {
        syncResults([]);
        asyncResults([]);
        return;
    }

    if (minervaVersion >= 14) {
        pluginContainer.find('.suggesting').removeClass('d-none');
    }


    if (query in globals.diseaseSuggestionsCache) {

        const suggestions = globals.diseaseSuggestionsCache[query];
        globals.lastSuggestions = suggestions;
        syncResults(Object.keys(suggestions));
        asyncResults([]);
        if (minervaVersion >= 14) {
            pluginContainer.find('.suggesting').addClass('d-none');
        }

    } else {

        syncResults([]);
        suggestDisease(query).then(function (suggestions) {
            globals.diseaseSuggestionsCache[query] = suggestions;
            globals.lastSuggestions = suggestions;
            asyncResults(Object.keys(suggestions));
            if (minervaVersion >= 14) {
                pluginContainer.find('.suggesting').addClass('d-none');
            }
        })
    }
};

const typeaheadSelect = function(ev, suggestion) {
    const diseaseIri = globals.lastSuggestions[suggestion].iri;
    ols.getDisease(diseaseIri).then(function(disease){

        globals.selectedDisease.setEfo(disease);
        globals.selectedDisease.setIri(disease);
        globals.selectedDisease.setOmims(disease);
        globals.selectedDisease.setSynonyms(disease);

        // const nameHtml = `<a href="${disease.iri}" target="_blank">${disease.label}</a>`;
        // const synonyms = 'synonyms' in disease && disease.synonyms !== null ? disease.synonyms : [];
        // const omims = 'OMIM_definition_citation' in disease.annotation ? disease.annotation.OMIM_definition_citation : [];
        // const omimsHtml = omims.map(o=>o.split(':')[1]).map(o => `<a href="https://www.omim.org/entry/${o}" target="_blank">${o}</a>`);
        //
        // pluginContainer.find('.disease-info.disease-name .disease-info-val').html(nameHtml);
        // pluginContainer.find('.disease-info.disease-synonyms .disease-info-val').text(synonyms.join(', '));
        // pluginContainer.find('.disease-info.disease-omim .disease-info-val').html(omimsHtml.join(', '));

        showDiseaseDetails();
        // pluginContainer.find('.disease-infos').show(400);
    });
};

function recreateBootstrapTables(){
    variationContainer.find('.bootstrap-table').remove();
    opentargetsContainer.find('.bootstrap-table').remove();
    $('<table class="variation-table" data-detail-view="true"></table>').appendTo(variationContainer);
    $('<table class="opentargets-table"></table>').appendTo(opentargetsContainer);
}

function suggestDisease(text){
    if (text === undefined) text = pluginContainer.find('.input-disease.tt-input').val();
    return ols.suggestDiseases(text);
}

function setTablesHeight() {
    setTableHeight(variationContainer.find('.variation-table'));
    setTableHeight(opentargetsContainer.find('.opentargets-table'));
}

function setTableHeight($table) {
    let $ftb = $table.closest('.fixed-table-body');
    //let top = $ftb.offset().top;
    let top = pluginContainer.find('.associations-content').offset().top;
    $ftb.css('height', `calc(100vh - ${top}px - 20px)`);
}

function highlightGeneOccurrences(geneName){
    return deHighlightAll().then(function () {
        let bioEntities = globals.gene2BioEntities[geneName];
        let highlightDefs = bioEntities.map(e => {
            return {
                element: {
                    id: e.id,
                    modelId: e.modelId,
                    type: "ALIAS"
                },
                type: "ICON"
            };
        });
        return minervaProxy.project.map.showBioEntity(highlightDefs);
    });
}

function registerTableEvents($table){
    $table.find('.fa-map-marker-alt').each((i, e) =>{
        e.onclick = function () {
            highlightGeneOccurrences(e.dataset.gene);
        };
    });
}

function searching(inProgress) {
    return inProgress ?
        '<i class="fa fa-circle-o-notch fa-spin"></i> Retrieving' :
        'Retrieve';
}

function activateSearchBtton(activate) {
    if (activate) {
        deHighlightAll();
        recreateBootstrapTables();
    }
    pluginContainer.find('.btn-search').html(searching(activate));
}

function initGene2BioEntities(){
    return minervaProxy.project.data.getAllBioEntities().then(function(bes){
        let aliases = bes.filter(be => be.constructor.name === 'Alias');
        aliases.forEach(a => {
            let ref = a.getReferences().filter(r=> r.getType() === 'HGNC_SYMBOL');
            if (ref.length > 0) {
                let hgncSymbol = ref[0].getResource();
                if (!(hgncSymbol in globals.gene2BioEntities)) globals.gene2BioEntities[hgncSymbol] = [];
                globals.gene2BioEntities[hgncSymbol].push({
                    id: a.getId(),
                    modelId: a.getModelId()
                });
            }
        });
    });
}

function search() {

    if (globals.searching) {
        return;
    }

    globals.searching = true;

    if (!globals.selectedDisease.getEfo() && !globals.selectedDisease.getOmims()) {
        showModal();
        return;
    }

    hideDiseaseDetails();
    hideErrorPanels();
    activateSearchBtton(true);

    const finishSearch = function() {
        globals.searching = false;
        activateSearchBtton(false);
    };

    Promise.all([queryForVariations(),  queryForOpenTargets()]).then(finishSearch, finishSearch);
}

function showModal() {
    pluginContainer.find(".modal").modal('show');
}

const geneFormatter = function (geneName) {
    let icon = geneName in globals.gene2BioEntities ?
        `<i class="fa fa-map-marker-alt" data-gene="${geneName}"></i>` :
        '';

    // return `${icon} ${geneName}`;
    return `${icon}`;
};

const customSort = function(sortName, sortOrder, data) {
    const order = sortOrder === 'desc' ? -1 : 1;
    data.sort(function (a, b) {
        let aa, bb;
        let aGeneInBioEntities = a.geneName in globals.gene2BioEntities;
        let bGeneInBioEntities = b.geneName in globals.gene2BioEntities;
        if (sortName === 'eye') {
            aa = aGeneInBioEntities;
            bb = bGeneInBioEntities;
        } else {
            aa = a[sortName];
            bb = b[sortName];
        }


        if (aa < bb) {
            return order * -1
        }
        if (aa > bb) {
            return order
        } else {
            if (aGeneInBioEntities < bGeneInBioEntities) {
                return 1;
            } else if (aGeneInBioEntities > bGeneInBioEntities){
                return -1;
            }

            return 0
        }

    })
};

const showErrorPanel = function($container, message){
    $container.find('.panel-body').html(message);
    $container.find('.error-panel').removeClass('hidden');
    $container.find('.error-panel').removeClass('d-none');
};

/********************** OPEN TAGETS **********************/

function queryForOpenTargets(){

    return queryOpenTargets().then( function(data) {

        if (data === undefined) {
            showErrorPanel(opentargetsContainer, 'No <a href="https://www.ebi.ac.uk/ols/ontologies/efo" target="_blank">EFO</a> ontology disease selected.');
            return;
        }

        let table = opentargetsContainer.find('.opentargets-table');
        table.bootstrapTable({
            columns: [{
                field: 'eye',
                title: '',
                sortable: true,
                formatter: geneFormatter,
                searchable: true
            },{
                field: 'geneName',
                title: 'Gene',
                sortable: true,
                // formatter: geneFormatter,
                searchable: true
            },{
                field: 'associationScore',
                title: 'Score',
                sortable: true
            },{
                field: 'id',
                title: 'Id',
                formatter: function(id){return `<a href="https://www.targetvalidation.org/evidence/${id.replace('-', '/')}" target="_blank">${id}</a>`},
                sortable: true
            }],
            data: data.data.map(d => {return {
                eye: d.target.gene_info.symbol,
                geneName: d.target.gene_info.symbol,
                associationScore:d.association_score.overall,
                id: d.id};
            }
            ),
            sortName: 'associationScore',
            sortOrder: 'desc',
            customSort: customSort,
            formatNoMatches: () => 'No association scoring above given threshold can be found in Open Targets.'
        });

        table.on('post-body.bs.table', function() {
            // sorting recreates objects, thus removing events
            // instead of using sort.bs.table or onSort, we need to used post-body, because sort.bs.table
            // is called before the actual sorting
            registerTableEvents(table);
        });
        setTableHeight(table);
        registerTableEvents(table);
    }, function(header, error) {
        showErrorPanel(opentargetsContainer, 'An error occured when connecting to the https://api.opentargets.io/v3/platform/public/association/filter endpoint.');
    });
}

const queryOpenTargets = function() {

    if (globals.selectedDisease.getEfo() === undefined) {
        return Promise.resolve(undefined)
    } else {
        const efoCode = globals.selectedDisease.getEfo().replace(':', '_');
        const minScore = 0.2;

        return $.ajax({url: `https://api.opentargets.io/v3/platform/public/association/filter?disease=${efoCode}&scorevalue_min=${String(minScore)}&size=10000`});
    }
};

/********************** VARIATION **********************/

function queryForVariations(){
    return queryUniprot().then( data => {
        // console.log("data", data);
        globals.groupedData = groupVariants(data);
        // console.log(globals.groupedData);
        let table = variationContainer.find('.variation-table');
        table.bootstrapTable({
            columns: [{
                field: 'eye',
                title: '',
                sortable: true,
                formatter: geneFormatter,
                searchable: true
            },{
                field: 'geneName',
                title: 'Gene',
                sortable: true,
                // formatter: geneFormatter,
                searchable: true
            },{
                field: 'occurrencesCnt',
                title: 'Occurrences',
                sortable: true
            },{
                field: 'isoformsCnt',
                title: 'Isoforms',
                sortable: true
            }, {
                field: 'organismName',
                title: 'Organism',
                sortable: true
            }],
            data: Object.values(globals.groupedData).map(d => {return {
                    eye:d[0].geneName,
                    geneName:d[0].geneName,
                    organismName:d[0].organismName,
                    occurrencesCnt: d[0].geneName in globals.gene2BioEntities ? globals.gene2BioEntities[d[0].geneName].length : 0,
                    isoformsCnt: d.length
                }; }
            ),
            sortName: 'occurrencesCnt',
            sortOrder: 'desc',
            customSort: customSort,
            onExpandRow: function (index, row, $detail) {
                let key = getKeyFromVariant(row);
                const tree = $(`<div id="tree-${key}"></div>`);
                tree.treeview({
                    data: getVariantsTree(globals.groupedData[key]),
                    enableLinks: true,
                    icon: 'fa',
                    collapseIcon: 'fa fa-minus',
                    expandIcon: 'fa fa-plus'
                });
                // tree.unbind("click"); //default
                $detail.append(tree);
            },
            formatNoMatches: () => 'No OMIM exists for given disease or no disease variant is known.'
        });

        table.on('post-body.bs.table', function() {
            // sorting recreates objects, thus removing events
            // instead of using sort.bs.table or onSort, we need to used post-body, because sort.bs.table
            // is called before the actual sorting
            registerTableEvents(table);
        });
        setTableHeight(table);
        registerTableEvents(table);
    }, function(header, error) {
        showErrorPanel(variationContainer, 'An error occured when connecting to the https://www.ebi.ac.uk/proteins/api/variation endpoint.');
    });
}

function queryUniprot() {

    const omims = globals.selectedDisease.getOmims().join(',');

    return omims === '' ? Promise.resolve([]) : $.ajax({url: `https://www.ebi.ac.uk/proteins/api/variation?offset=0&size=-1&omim=${omims}`});

    // return $.ajax({
    //     url: `https://www.ebi.ac.uk/proteins/api/variation?offset=0&size=-1&disease=${disease}`
    // })
}

function isString(s) {
    return typeof(s) === 'string' || s instanceof String;
}

function getVariantsTree(variants) {

    function getLevel(obj, name) {

        if (obj instanceof Array) {
            return obj.map((o,i) => {
                return {
                    text: `${name}  ${i+1}`,
                    nodes: getLevel(o)
                }
            })
        }

        const l = [];
        let i = 0;
        for (let k in obj) {
            if (obj[k] instanceof Array || obj[k] instanceof Object) {
                l.push({
                    text: k,
                    nodes: getLevel(obj[k], k.replace(/s *$/, '')),
                    state: {expanded: globals.expanded}
                });

            } else {
                let linkified = isString(obj[k]) ? linkify(obj[k], {attributes: {target:'_blank'}}) : obj[k];
                l.push({
                    text: `${k}: ${linkified}`,
                    selectable: false
                });
            }

        }

        return l;
    }

    return variants.map(v => { return {
        text: v.entryName,
        nodes: getLevel(v),
        state: {expanded: globals.expanded}
    };});

}

function getKeyFromVariant(v) {
    return `${v.geneName}-${v.organismName.replace(" ", "_")}`;
}

function groupVariants(data) {
    const res = {};

    for (let i in data) {
        const d = data[i];
        const key = getKeyFromVariant(d);
        if (!(key in res)) res[key] = [];
        res[key].push(d);
    }

    return res;
}
