const $ = require('jquery');

const getDisease = function(iri){

    const encodedIri = encodeURIComponent(encodeURIComponent(iri));

    return $.ajax({
        url: `https://www.ebi.ac.uk/ols/api/ontologies/efo/terms/${encodedIri}`
    });
};

const cache = {};
let urls = [];
const suggestDiseases = function (text) {

    const caseInsensitive = function(value, index, self){

        const iVal = value.autosuggest.toLowerCase();
        for (let i = 0; i < self.length; i++){
            if (self[i].autosuggest.toLowerCase() === iVal) {
                return i === index;
            }
        }
    };

    let suggestedDisDesc = []; //array of suggestion-disease description pair

    return $.ajax({
        url: `https://www.ebi.ac.uk/ols/api/suggest?q=${encodeURIComponent(text)}&ontology=efo&rows=20`
    }).then(function (res) {
        const suggestions = res.response.docs
            .filter(caseInsensitive)
            .map(d => {
                suggestedDisDesc.push({suggestion: d.autosuggest, desc: undefined});
                return d.autosuggest;
            });
        const promises =  suggestions.map(s => $.ajax({url: `https://www.ebi.ac.uk/ols/api/search?q=${encodeURIComponent(s)}&ontology=efo&queryFields=label,synonym&exact=true`}))
        return Promise.all(promises);
    }).then(function (ress) {

        for (let i = ress.length -1; i >=0; i--) {
            const res = ress[i];
            if (res.response.docs.length > 0 && 'obo_id' in res.response.docs[0]) {
                suggestedDisDesc[i].desc = res.response.docs[0];
            } else {
                suggestedDisDesc.splice(i, 1);
            }
        }

        urls = [];
        const promises = suggestedDisDesc
            .map((sdd, i) =>  {
                const url = `https://www.ebi.ac.uk/ols/api/ontologies/efo/ancestors?id=${encodeURIComponent(sdd.desc.obo_id)}`;
                urls.push(url);
                if (Object.keys(cache).indexOf(url) >= 0) {
                    console.log("cahce");
                    return Promise.resolve(cache[url]);
                }
                else
                    return $.ajax({url: url})
            });

        return Promise.all(promises);

    }).then(function (ress) {

        const sd = {};

        //not each of the EFO are diseases so we need to filter those out
        suggestedDisDesc.forEach(function (sdd, i) {
            cache[urls[i]] = ress[i];
            if (ancestorsContainDisease(ress[i])) {
                sd[sdd.suggestion] = sdd.desc;
            }
        });

        return sd;
    })
};

const ancestorsContainDisease = function(ancestorsJson) {

    let isDisease = false;
    //EFO:0000408 = disease
    ancestorsJson._embedded.terms.forEach(t => isDisease = isDisease || (t.obo_id === "EFO:0000408"));
    return isDisease;
};

module.exports = {
    suggestDiseases: suggestDiseases
    , getDisease: getDisease
};