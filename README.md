# Disease associations

`Disease associations` is a plugin allowing one to see genes associated with given disease
in the context of selected disease map. Currently, the plugin shows genes associations obtained
from [Protein API variation service](https://www.ebi.ac.uk/proteins/api/doc/) (includes also
information about specific variants)
 and [Open Targets API](https://docs.targetvalidation.org/tutorials/rest-api).
 
**Please bear in mind that the mapping between obtained genes and genes found in the map
 is based on HGNC. So if entities in the map do not contain HGNC annotations, 
there will be no mapping available, i.e. you won't see any eye icons in the tables.** 
 
 ![Plugin example](img/printscreen.png)
 


### General instructions

In order to use the precompiled and publicly available version of the plugin, 
open the plugin menu in the MINERVA's upper left corner (see image below) and click plugins.
In the dialog which appears enter the following address in the URL box: `https://minerva-dev.lcsb.uni.lu/plugins/disease-associations/plugin.js` .
The plugin shows up in the plugins panel on the right hand side of the screen.

### Plugin functionality

#### Obtaining disease-gene associations

- The users picks whether she wants to input disease based on [EFO](https://www.ebi.ac.uk/efo/) ontology 
or [OMIM](https://www.omim.org/) ontology (see 
[Variation information data source section](#Variation-information)
for details).
- In case EFO is selected, when the users starts 
inputting the disease of interest in the disease input box the plugin 
starts suggesting available diseases from the [EFO ontology](https://www.ebi.ac.uk/efo/) 
via the [EBI's ontology lookup service](https://www.ebi.ac.uk/ols/docs/api)
- After a disease is selected, the plugin shows details of the disease including its
synonyms and mapped [OMIM](https://www.omim.org/) ontology terms.
- In case OMIM is selected, the user needs to input OMIM ID and no additional details 
are shown.
- After clicking on the <i>Retrieve</i> button,  
the plugin connects to the [Protein API variation service](https://www.ebi.ac.uk/proteins/api/doc/) 
and [Open Targets API](https://docs.targetvalidation.org/tutorials/rest-api) 
and retrieves all available disease-gene associations. In case of Protein API, the associations are 
based on the OMIM while in case of Open Targets, the associations are based directly on 
the EFO ID which was used to provide the disease details.

#### Disease-gene associations inspection

The obtained associations are shown in tabs based on the the source of information.

##### 1. Variation information from Protein API variation service

The variation service is used to obtain disease-associated variants retrieved from large-scale
genomics projects (see details in the respective section below).

- Returned variants are grouped by isoforms of genes they are involved in. 
The plugin groups variants in isoforms by gene name
and displays them in a table where each row contains the gene name, number of occurrences of that gene in
the map and number of isoforms.
- The user can click on the eye icon of a particular gene to show its occurrences in the map. 
If there is no occurrence of given gene in the map, 
the number of occurrences is zero and no eye icon is displayed.
- By clicking on the plus symbol in each line line, one can explore all the data which was obtained from 
the Protein API for that gene. 

##### 2. Associations from Open Targets

Open Targets platform is used to obtain disease-gene associations including 
an [aggregated association score](https://docs.targetvalidation.org/getting-started/scoring) showing the 
strength of the respective association. Only associations with score > 0.2 are retrieved and displayed.
Moreover, only top 10.000 associations are retrieved, which probably will be enough for any disease.

- Each association contains the involved gene, strength of the association and link to Open Targets
where one can inspect detailed information about the particular association.
- The user can click on the eye icon of a particular gene to show its occurrences in the map. 
If there is no occurrence of given gene in the map, no eye icon is displayed.


### Data resources

### Ontology Lookup Service (OLS)

EBI's [OLS](https://www.ebi.ac.uk/ols/index) is used to obtain disease 
[suggestions](https://www.ebi.ac.uk/ols/docs/api#_suggest_term) and
[disease details]((https://www.ebi.ac.uk/ols/docs/api#resources-term)) from the
[EFO ontology]((https://www.ebi.ac.uk/efo/)), one of the ontologies under the OLS umbrella.


###Variation information

The information about variants comes from EBI's [Proteins API](https://www.ebi.ac.uk/proteins/api/doc/#!/variation/search).
As stated on the pages of Proteins API *"The variation, proteomics and antigen services 
provide annotations imported and mapped from large scale data sources, 
such as 1000 Genomes, ExAC (Exome Aggregation Consortium), 
TCGA (The Cancer Genome Atlas), COSMIC (Catalogue Of Somatic Mutations In Cancer), 
PeptideAtlas, MaxQB (MaxQuant DataBase), EPD (Encyclopedia of Proteome Dynamics) and HPA, 
along with UniProtKB annotations for these feature types (if applicable)"*. 

To specify the disease of interest, the Proteins API can be passed either a text or list of
[OMIM](https://www.omim.org) identifiers. The plugin uses the latter option since 
the EFO ontology includes also a mapping from EFO to OMIM. For example, 
[Parkinson's disease](https://www.ebi.ac.uk/ols/ontologies/efo/terms?iri=http%3A%2F%2Fwww.ebi.ac.uk%2Fefo%2FEFO_0002508)
is mapped to OMIM:616710, OMIM:616361, OMIM:168600. We should mention here that 
OMIM ontology seems to have a more granular distinction of diseases and thus for example
it contains Parkinson disease 1-10 which do not have exact mapping in EFO.  

The following image shows an example of the data (a snippet from Google Chrome DevTools) 
already grouped by gene name and species information (see above).

![Grouped Proteins API data](img/data_example.png)

### Open Targets

Open Targets platform supports workflows starting 
from a target or disease, and shows the available evidence for target – disease associations. 
The plugin uses the [Open Targets Platform REST-API](https://docs.targetvalidation.org/tutorials/rest-api)
to obtain the associations and their scores.